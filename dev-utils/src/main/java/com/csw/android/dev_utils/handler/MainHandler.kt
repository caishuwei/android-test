@file:Suppress(
    "MemberVisibilityCanBePrivate", "TYPE_INFERENCE_ONLY_INPUT_TYPES_WARNING",
    "EqualsOrHashCode", "unused"
)
package com.csw.android.dev_utils.handler

import android.os.Handler
import android.os.Looper

/**
 * 主线程消息处理器
 */
class MainHandler(callback: Callback? = null) : Handler(Looper.getMainLooper(), callback)