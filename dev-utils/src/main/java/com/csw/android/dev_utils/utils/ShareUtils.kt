package com.csw.quickmvp.utils

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.text.TextUtils

object ShareUtils {
    /**
     * 分享图片
     * 貌似带了uri之后分享过去的只有图片，操蛋啊
     * 7.0手机还是用旧版本QQ的话，无法解析FileProvider产生的Uri，导致图片无法读取
     * 设置类型text/plain时，qq是发送图片文件，而不是发送一张图片
     */
    fun shareImage(activity: Activity, uri: Uri?, text: String?) {
        try {
            val shareIntent = Intent(Intent.ACTION_SEND)
            shareIntent.setType("image/*")
            shareIntent.putExtra(Intent.EXTRA_STREAM, uri)
            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            if (!TextUtils.isEmpty(text)) {
                shareIntent.putExtra("sms_body", text)
                shareIntent.putExtra(Intent.EXTRA_TEXT, text)
            }
            activity.startActivity(Intent.createChooser(shareIntent, "Share with..."))
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun shareVideo(activity: Activity, uri: Uri?, text: String?) {
        try {
            val shareIntent = Intent(Intent.ACTION_SEND)
            shareIntent.setType("video/*")
            shareIntent.putExtra(Intent.EXTRA_STREAM, uri)
            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            if (!TextUtils.isEmpty(text)) {
                shareIntent.putExtra("sms_body", text)
                shareIntent.putExtra(Intent.EXTRA_TEXT, text)
            }
            activity.startActivity(Intent.createChooser(shareIntent, "Share with..."))
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}