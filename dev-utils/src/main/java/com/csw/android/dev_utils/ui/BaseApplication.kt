package com.csw.android.dev_utils.ui

import android.app.Application
import com.csw.android.dev_utils.SDK

open class BaseApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        SDK.init(this)
    }
}