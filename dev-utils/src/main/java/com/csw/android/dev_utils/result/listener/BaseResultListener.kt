package com.csw.android.dev_utils.result.listener

interface BaseResultListener {

    /**
     * 操作取消了
     */
    fun onCancel()

}