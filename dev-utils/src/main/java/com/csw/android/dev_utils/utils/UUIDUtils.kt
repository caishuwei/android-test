package com.csw.android.dev_utils.utils

import java.util.*

object UUIDUtils {

    /**
     * 生成一个唯一ID
     */
    fun generateId(): String {
        return UUID.randomUUID().toString().replace("-", "")
    }

}