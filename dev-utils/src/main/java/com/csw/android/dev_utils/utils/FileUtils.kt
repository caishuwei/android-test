@file:Suppress("unused", "MemberVisibilityCanBePrivate", "RedundantOverride")

package com.csw.android.dev_utils.utils

import java.io.File
import java.io.IOException

object FileUtils {

    /**
     * 生成文件夹
     */
    fun generateDirectory(path: String): File {
        val file = File(path)
        if (!file.exists()) {
            file.mkdirs()
        }
        return file
    }

    /**
     * 生成文件
     */
    fun generateFile(path: String): File? {
        val file = File(path)
        if (!file.exists()) {
            try {
                file.createNewFile()
            } catch (e: IOException) {
                e.printStackTrace()
                return null
            }
        }
        return file
    }

    fun generateFile(file: File): File? {
        if (!file.exists()) {
            try {
                file.createNewFile()
            } catch (e: IOException) {
                e.printStackTrace()
                return null
            }
        }
        return file
    }

    /**
     * 清理文件夹下的文件
     */
    fun clearDirectory(dirPath: String) {
        val file: File = generateDirectory(dirPath)
        if (file.exists()) {
            file.listFiles()?.run {
                for (f in this) {
                    deleteFile(f)
                }
            }
        }
    }

    /**
     * 删除文件/文件夹，若包含的子文件过多，可考虑子线程调用。。。
     */
    fun deleteFile(file: File?) {
        if (file != null && file.exists()) {
            if (file.isDirectory) {
                file.listFiles()?.run {
                    for (f in this) {
                        deleteFile(f)
                    }
                }
            }
            file.delete()
        }
    }

}