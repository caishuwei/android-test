@file:Suppress("MemberVisibilityCanBePrivate", "UNUSED_PARAMETER")
package com.csw.android.dev_utils.entities

import android.database.Cursor
import android.provider.MediaStore
import java.io.Serializable

class FileInfo(val id: Int, val filePath: String) : Serializable {
    companion object {
        fun fromCursor(cursor: Cursor): FileInfo? {
            try {
                cursor.run {
                    val path = getString(getColumnIndex(MediaStore.Files.FileColumns.DATA))
                    val id = getInt(getColumnIndex(MediaStore.Files.FileColumns._ID))
                    return FileInfo(id, path)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return null
        }
    }

    val uri: String = filePath
    val fileName: String

    init {
        val lastNode = filePath.lastIndexOf('/')
        if (lastNode >= 0 && lastNode < filePath.length - 1) {
            fileName = filePath.substring(lastNode + 1, filePath.length)
        } else {
            fileName = filePath
        }
    }
}