package com.csw.android.dev_utils.result.listener

import android.net.Uri
import com.csw.android.dev_utils.result.listener.BaseResultListener

/**
 * uri请求结果
 */
interface OnUriResultListener : BaseResultListener {
    /**
     * 用户选择后返回一个uri
     */
    fun onComplete(uri: Uri)
}