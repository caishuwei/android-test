@file:Suppress("unused", "MemberVisibilityCanBePrivate", "RedundantOverride")
package com.csw.android.dev_utils.utils

import androidx.fragment.app.FragmentManager
import com.csw.android.dev_utils.result.ImageSelectorFragment
import com.csw.android.dev_utils.result.listener.OnUriResultListener

object FileSelectUtils {

    /**
     * 选择一张图片
     */
    fun selectImage(fragmentManager: FragmentManager, listener: OnUriResultListener) {
        FragmentHelper.getFragmentInstance(
            fragmentManager,
            ImageSelectorFragment::class.java.name,
            ImageSelectorFragment::class.java
        ).execute(listener)
    }

}