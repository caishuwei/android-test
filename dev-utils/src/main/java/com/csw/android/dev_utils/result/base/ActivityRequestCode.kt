package com.csw.android.dev_utils.result.base

enum class ActivityRequestCode(val code: Int,val desc: String) {
    IMAGE_SELECTOR(1001, "图片选择器")
}