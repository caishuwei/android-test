// RemoteServiceAIDLInterface.aidl
package com.csw.android.androidtest;

// Declare any non-default types here with import statements
/**
 * AIDL是android提供给远程服务的通信机制，跨进程但可以阻塞同步调用，调用线程阻塞等候另一个进程执行完方法返回，
 * 唤醒线程。
 * 底层基于android的binder机制实现，由于跨进程，所以往来的参数必须可以序列化和反序列化，基本类型都是可以使用，
 * 其它的类型可以使用Parcelable实现序列化。
 * AIDL接口文件可以拷贝到别的app，这样两个app拥有同样的AIDL文件即可建立起通信，但不太看好，万一别app升级修改
 * 了，不就被绑定也得跟着改吗。。。当然同个app内不同进程使用是妥妥的。。。
 *
 * 编写完AIDL接口，编译一下自动生成类文件
 */
interface RemoteServiceAIDLInterface {

    String execCommand1(String data) ;

    String execCommand2();
}