package com.csw.android.androidtest.module.animator;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.widget.Button;

import com.csw.android.androidtest.R;
import com.csw.android.dev_utils.ui.BaseFragment;
import com.csw.android.dev_utils.ui.CommonActivity;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * 动画测试
 */
public class AnimationTest extends BaseFragment {

    public static void open(Context context) {
        CommonActivity.Companion.openActivity(context, AnimationTest.class, null);
    }

    protected View imageView;
    private Button button;

    @Override
    public int getContentViewID() {
        return R.layout.fragment_animation_test;
    }

    @Override
    public void initView(@NotNull View rootView, @Nullable Bundle savedInstanceState) {
        super.initView(rootView, savedInstanceState);
        imageView = rootView.findViewById(R.id.imageView);
        button = rootView.findViewById(R.id.button);
    }

    @Override
    public void initListener() {
        super.initListener();
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startAnimation();
            }
        });
    }

    protected void startAnimation() {
        execStartAnimation();
    }

    private void execStartAnimation() {
        AnimationSet animationSet = new AnimationSet(false);
        animationSet.setInterpolator(new AccelerateInterpolator());
        ScaleAnimation scaleAnimation = new ScaleAnimation(
                1,
                2,
                1,
                2,
                Animation.RELATIVE_TO_SELF,
                0.5f,
                Animation.RELATIVE_TO_SELF,
                0.5f
        );
        animationSet.addAnimation(scaleAnimation);
        RotateAnimation rotateAnimation = new RotateAnimation(
                0,
                360,
                Animation.RELATIVE_TO_SELF,
                0.5f,
                Animation.RELATIVE_TO_SELF,
                0.5f
        );
        animationSet.addAnimation(rotateAnimation);
        animationSet.setDuration(500);
        animationSet.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                execEndAnimation();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        imageView.startAnimation(animationSet);
    }

    private void execEndAnimation() {
        AnimationSet animationSet = new AnimationSet(false);
        animationSet.setInterpolator(new DecelerateInterpolator());
        ScaleAnimation scaleAnimation = new ScaleAnimation(
                2,
                1,
                2,
                1,
                ScaleAnimation.RELATIVE_TO_SELF,
                0.5f,
                ScaleAnimation.RELATIVE_TO_SELF,
                0.5f
        );
        animationSet.addAnimation(scaleAnimation);
        animationSet.setDuration(500);
        imageView.startAnimation(animationSet);
    }

    @Override
    public void onDestroyView() {
        imageView = null;
        button = null;
        super.onDestroyView();
    }
}
