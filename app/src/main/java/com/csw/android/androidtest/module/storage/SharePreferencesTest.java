package com.csw.android.androidtest.module.storage;

import android.content.Context;

import com.csw.android.androidtest.entities.Student;
import com.csw.android.androidtest.ui.LogViewFragment;
import com.csw.android.dev_utils.utils.GSONUtils;
import com.csw.android.dev_utils.utils.SharedPreferencesUtils;
import com.csw.android.dev_utils.utils.ToastUtils;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * 本地存储都是基于文件存储，简单粗暴的文件读写一般一个文件只能存一份配置
 * sharePreferences 是android基于文件存储提供的一个键值对存储api，用于存储可序列化类型的数据
 */
public class SharePreferencesTest extends LogViewFragment {

    @Override
    public void initData() {
        super.initData();
        Student student = new Student("蔡树伟", "男", 28);

        saveStudent(student);
    }

    private void saveStudent(Student student) {
        Observable.just(student)
                .observeOn(Schedulers.io())
                .doOnNext(new Consumer<Student>() {
                    @Override
                    public void accept(Student student) throws Exception {
                        //进行序列化便于存储
                        String dataStr = GSONUtils.INSTANCE.toJSONString(student);
                        //config 私有目录
                        SharedPreferencesUtils.Companion.getSharedPreferencesEditor("config", Context.MODE_PRIVATE)
                                .putString("student", dataStr)
                                .commit();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError(new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        throwable.printStackTrace();
                        ToastUtils.INSTANCE.showShort("存储失败", ToastUtils.INSTANCE.getDEFAULT_TOAST());
                    }
                })
                .doOnComplete(new Action() {
                    @Override
                    public void run() throws Exception {
                        readStudent();
                    }
                }).subscribe();

    }

    private void readStudent() {
        Observable.create(new ObservableOnSubscribe<Student>() {
            @Override
            public void subscribe(@NonNull ObservableEmitter<Student> emitter) throws Exception {
                String dataStr = SharedPreferencesUtils.Companion.getSharedPreferences("config", Context.MODE_PRIVATE)
                        .getString("student", null);
                Student student = GSONUtils.INSTANCE.parseObject(dataStr, Student.class);
                if (!emitter.isDisposed()) {
                    emitter.onNext(student);
                }
                if (!emitter.isDisposed()) {
                    emitter.onComplete();
                }
            }
        })
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(new Consumer<Student>() {
                    @Override
                    public void accept(Student student) throws Exception {
                        logView.addLog(student.getName());
                        logView.addLog(student.getSex());
                        logView.addLog(student.getAge());
                    }
                })
                .doOnError(new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        throwable.printStackTrace();
                        ToastUtils.INSTANCE.showShort("读取失败", ToastUtils.INSTANCE.getDEFAULT_TOAST());
                    }
                })
                .subscribe();
    }

}