package com.csw.android.androidtest.net.socket.base;

public interface SocketListener {

    /**
     * socket准备完成
     *
     * @param ip   当前socket使用的ip
     * @param port 当前socket使用的端口
     */
    void onPrepared(String ip, int port);

    /**
     * 异常结束
     */
    void onError(Exception e);

    /**
     * 正常结束
     */
    void onClose();

    /**
     * 接收一条消息
     * @param data 消息数据
     */
    void onReceiveMessage(byte[] data);
}
