package com.csw.android.androidtest.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import androidx.annotation.Nullable;

/**
 * 这里展示的是bindService在进程内的使用，start方式不获取服务实例，所以整个任务执行过程是异步的，任务结果通过
 * 事件订阅框架通知。而bindService则是可以获取对象，然后以阻塞同步的方式执行任务，当然也可以选择异步实现。
 */
public class BindService extends Service {

    @Override
    public void onCreate() {
        super.onCreate();
        //初始化
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return new MyBinder();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    @Override
    public void onDestroy() {
        //状态保存
        super.onDestroy();
    }

    //对外公开方法-----------------------------------------------------------------------------------
    public String execCommand1(String data) {
        return "BindService is receive " + data;
    }

    public String execCommand2() {
        return "BindService execCommand2";
    }

    //----------------------------------------------------------------------------------------------
    public final class MyBinder extends Binder {
        public BindService getServiceInstance() {
            return BindService.this;
        }
    }
}
