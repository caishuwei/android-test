package com.csw.android.androidtest.module.animator;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.view.animation.AccelerateDecelerateInterpolator;

import androidx.core.view.ViewCompat;

/**
 * 属性动画
 */
public class AnimatorTest extends AnimationTest {

    @Override
    protected void startAnimation() {
        ObjectAnimator scaleX = ObjectAnimator.ofFloat(imageView, "scaleX", 1, 2);
        ObjectAnimator scaleY = ObjectAnimator.ofFloat(imageView, "scaleY", 1, 2);
        ObjectAnimator rotation = ObjectAnimator.ofFloat(imageView, "rotation", 0, 360);
        ObjectAnimator scaleX2 = ObjectAnimator.ofFloat(imageView, "scaleX", 2, 1);
        ObjectAnimator scaleY2 = ObjectAnimator.ofFloat(imageView, "scaleY", 2, 1);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.play(scaleX).with(scaleY).with(rotation);
        animatorSet.play(scaleX2).with(scaleY2).after(scaleX);
        animatorSet.setInterpolator(new AccelerateDecelerateInterpolator());
        animatorSet.setDuration(500);
        animatorSet.start();
    }
}
