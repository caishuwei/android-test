package com.csw.android.androidtest.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.csw.android.androidtest.R;
import com.csw.android.androidtest.module.animator.AnimationTest;
import com.csw.android.androidtest.module.animator.AnimatorTest;
import com.csw.android.androidtest.module.event.BroadcastTest;
import com.csw.android.androidtest.module.event.LocalBroadcastTest;
import com.csw.android.androidtest.module.event.RxBusTest;
import com.csw.android.androidtest.module.fragment.FragmentForLifecycleListenerTest;
import com.csw.android.androidtest.module.fragment.FragmentForSystemCallbackTest;
import com.csw.android.androidtest.module.fragment.FragmentForUITest;
import com.csw.android.androidtest.module.net.socket.UDPSocketTest;
import com.csw.android.androidtest.module.rxjava.RxjavaBaseTest;
import com.csw.android.androidtest.module.serialize.JsonTest;
import com.csw.android.androidtest.module.serialize.ParcelableTest;
import com.csw.android.androidtest.module.serialize.SerializableTest;
import com.csw.android.androidtest.module.servive.BindServiceTest;
import com.csw.android.androidtest.module.servive.ForegroundServiceTest;
import com.csw.android.androidtest.module.servive.RemoteServiceTest;
import com.csw.android.androidtest.module.servive.StartServiceTest;
import com.csw.android.androidtest.module.storage.DatabaseTest;
import com.csw.android.androidtest.module.storage.FileStorageTest;
import com.csw.android.androidtest.module.storage.SharePreferencesTest;
import com.csw.android.androidtest.module.thread.ExecutorsTest;
import com.csw.android.androidtest.module.thread.HandlerThreadTest;
import com.csw.android.androidtest.module.thread.ThreadTest;
import com.csw.android.androidtest.module.thread.ToMainThreadTest;
import com.csw.android.androidtest.module.view.CircleSeekBarTest;
import com.csw.android.androidtest.module.view.ComposeViewTest;
import com.csw.android.androidtest.module.view.CustomViewStepTest;
import com.csw.android.androidtest.module.view.ExtendsViewTest;
import com.csw.android.androidtest.module.webview.WebViewTest;
import com.csw.android.dev_utils.ui.CommonActivity;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new MenuAdapter());
    }

    private static class MenuAdapter extends RecyclerView.Adapter<MenuViewHolder> {

        private final List<Object> data = new ArrayList<>();

        public MenuAdapter() {
            data.add("animator");
            {
                data.add(new Item(AnimationTest.class));
                data.add(new Item(AnimatorTest.class));
            }
            data.add("view");
            {
                data.add(new Item(ComposeViewTest.class));
                data.add(new Item(ExtendsViewTest.class));
                data.add(new Item(CustomViewStepTest.class));
                data.add(new Item(CircleSeekBarTest.class));
            }
            data.add("thread");
            {
                data.add(new Item(ToMainThreadTest.class));
                data.add(new Item(ThreadTest.class));
                data.add(new Item(HandlerThreadTest.class));
                data.add(new Item(ExecutorsTest.class));
            }
            data.add("rxjava");
            {
                data.add(new Item(RxjavaBaseTest.class));
            }
            data.add("event");
            {
                data.add(new Item(BroadcastTest.class));
                data.add(new Item(LocalBroadcastTest.class));
                data.add(new Item(RxBusTest.class));
            }
            data.add("serialize");
            {
                data.add(new Item(SerializableTest.class));
                data.add(new Item(ParcelableTest.class));
                data.add(new Item(JsonTest.class));
            }
            data.add("service");
            {
                data.add(new Item(StartServiceTest.class));
                data.add(new Item(ForegroundServiceTest.class));
                data.add(new Item(BindServiceTest.class));
                data.add(new Item(RemoteServiceTest.class));
            }
            data.add("storage");
            {
                data.add(new Item(FileStorageTest.class));
                data.add(new Item(SharePreferencesTest.class));
                data.add(new Item(DatabaseTest.class));
            }
            data.add("webview");
            {
                data.add(new Item(WebViewTest.class));
            }
            data.add("net");
            {
                data.add(new Item(UDPSocketTest.class));
            }
            data.add("fragment");
            {
                data.add(new Item(FragmentForUITest.class));
                data.add(new Item(FragmentForSystemCallbackTest.class));
                data.add(new Item(FragmentForLifecycleListenerTest.class));
            }
        }

        @Override
        public int getItemViewType(int position) {
            if (data.get(position) instanceof String) {
                return 1;
            } else if (data.get(position) instanceof Item) {
                return 2;
            }
            return super.getItemViewType(position);
        }

        @NonNull
        @Override
        public MenuViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            switch (viewType) {
                case 1:
                    return new MenuViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_main_menu_t1, parent, false));
                case 2:
                    return new MenuViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_main_menu_t2, parent, false));
                default:
                    throw new RuntimeException("unknown view type");
            }
        }

        @Override
        public void onBindViewHolder(@NonNull MenuViewHolder holder, int position) {
            switch (getItemViewType(position)) {
                case 1:
                    holder.textView.setText(data.get(position).toString());
                    break;
                case 2:
                    Item item = (Item) data.get(position);
                    holder.textView.setText(item.getName());
                    holder.textView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            item.handleClick(v.getContext());
                        }
                    });
                    break;
            }
        }

        @Override
        public int getItemCount() {
            return data.size();
        }
    }

    private static class MenuViewHolder extends RecyclerView.ViewHolder {
        public final TextView textView;

        public MenuViewHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.textView);
        }
    }

    private static class Item {
        private Class clazz;

        public Item(Class clazz) {
            this.clazz = clazz;
        }

        public String getName() {
            return clazz.getSimpleName();
        }

        public void handleClick(Context context) {
            switch (getClassType(clazz)) {
                default:
                case 0:
                    break;
                case 1:
                    context.startActivity(new Intent(context, clazz));
                    break;
                case 2:
                    CommonActivity.Companion.openActivity(context, clazz, null);
                    break;
            }
        }

        private int getClassType(Class c) {
            if (c == null) {
                return 0;
            } else if (c == Activity.class) {
                return 1;
            } else if (c == Fragment.class) {
                return 2;
            } else {
                return getClassType(c.getSuperclass());
            }
        }
    }
}
