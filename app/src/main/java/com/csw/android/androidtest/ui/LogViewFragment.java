package com.csw.android.androidtest.ui;

import android.os.Bundle;
import android.view.View;

import com.csw.android.androidtest.R;
import com.csw.android.androidtest.view.LogView;
import com.csw.android.dev_utils.ui.BaseFragment;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class LogViewFragment extends BaseFragment {
    protected LogView logView;

    @Override
    public int getContentViewID() {
        return R.layout.fragment_log_view;
    }

    @Override
    public void initView(@NotNull View rootView, @Nullable Bundle savedInstanceState) {
        super.initView(rootView, savedInstanceState);
        logView = rootView.findViewById(R.id.logView);
    }

    @Override
    public void onDestroyView() {
        logView = null;
        super.onDestroyView();
    }
}
