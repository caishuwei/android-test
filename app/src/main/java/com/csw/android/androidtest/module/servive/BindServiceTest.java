package com.csw.android.androidtest.module.servive;

import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;

import com.csw.android.androidtest.app.MyApplication;
import com.csw.android.androidtest.service.BindService;
import com.csw.android.androidtest.service.StartService;
import com.csw.android.androidtest.ui.LogViewFragment;
import com.csw.android.dev_utils.utils.RxBus;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;

/**
 * 以bindService的方式启动服务
 */
public class BindServiceTest extends LogViewFragment {
    private BindService bindServiceInstance = null;

    private final ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            if (service instanceof BindService.MyBinder) {
                bindServiceInstance = ((BindService.MyBinder) service).getServiceInstance();
                logView.addLog(bindServiceInstance.execCommand1("12345"));
                logView.addLog(bindServiceInstance.execCommand2());
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            bindServiceInstance = null;
        }
    };

    @Override
    public void initData() {
        super.initData();
        //绑定服务获取实例，可以看出这一步是异步的，必须等候服务创建完毕并回调才会获取到实例，
        //bingService可能比startService生存能力更强一点，毕竟跟界面绑定的时候优先级应该挺高的。
        //可以尝试在app启动页bindService的方式启动服务，获取到实例后将实例存入Application中，无需解绑，生命
        //周期跟随app，这样整个应用都可以访问到服务的实例，同步调用方法，不过如果要这么做为何还要去用服务实现
        // ，闲的蛋疼，直接单例实现功能不就可以了，哈哈哈。
        //bindService的设计想法应该是，在bind的时候存活处理任务，在所有绑定者都解绑的时候关闭服务，如果单例
        //那就是永久存在占用资源，想法很好，但bindService是异步获取操作对象，这就很让人难以接受，ui还得等对象
        //获取后才能去调用执行方法，那我情愿用start方式直接整个过程异步算了。
        MyApplication.instance.bindService(new Intent(MyApplication.instance, BindService.class), serviceConnection, Service.BIND_AUTO_CREATE);
    }

    @Override
    public void onDestroy() {
        MyApplication.instance.unbindService(serviceConnection);
        super.onDestroy();
    }
}
