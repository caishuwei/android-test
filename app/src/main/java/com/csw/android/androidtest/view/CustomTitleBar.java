package com.csw.android.androidtest.view;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.csw.android.androidtest.R;

/**
 * 封装一个标题栏
 */
public class CustomTitleBar extends FrameLayout {

    private TextView title;

    public CustomTitleBar(@NonNull Context context) {
        super(context);
        init();
    }

    public CustomTitleBar(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomTitleBar(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.view_custom_titlebar, this);
        //实现退出activity功能
        findViewById(R.id.back).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                final Context c = getContext();
                if (c instanceof Activity) {
                    ((Activity) c).finish();
                }
            }
        });
        title = findViewById(R.id.title);
    }

    /**
     * 对外提供设置标题功能
     */
    public void setTitle(String title) {
        this.title.setText(title);
    }
}
