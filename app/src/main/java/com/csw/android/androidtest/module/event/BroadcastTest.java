package com.csw.android.androidtest.module.event;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.csw.android.androidtest.ui.LogViewFragment;

/**
 * 广播测试
 * <p>
 * 广播是android四大组件之一，也是跨进程的，所以使用时需要注意加以限制，否则会有多个程序接收到同个广播，也可能
 * 接收到别人程序的广播，所以用起来比较麻烦，需要加上诸多判断。
 */
public class BroadcastTest extends LogViewFragment {

    /**
     * 广播接收器
     */
    private final BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //若接收到一个有序广播的话，可以通过接收器的abortBroadcast拦截后续接收器接收广播
//            receiver.abortBroadcast();
            if (intent != null) {
                //该广播可能是别的程序发出来的，我们并不想处理别的程序的广播的话，加一下判断，当然这里不排除
                //有别的程序通过指定包名发送广播给我们
                String pn = intent.getPackage();
                if (context.getPackageName().equals(pn)) {
                    String action = intent.getAction();
                    if (action != null) {
                        switch (action) {
                            case "BroadcastTest1":
                                String v1 = intent.getStringExtra("key1");
                                int v2 = intent.getIntExtra("key2", 0);
                                double v3 = intent.getDoubleExtra("key3", 0.0);
                                //因为有可能是别的程序通过相同权限和action发送过来的数据，所以无法确定他是否添加以上这些数据，所以都得判断一下
                                if (v1 != null) {
                                    logView.addLog(v1);
                                }
                                if (v2 != 0) {
                                    logView.addLog(v2);
                                }
                                if (v3 != 0) {
                                    logView.addLog(v3);
                                }
                                break;
                            case "BroadcastTest2":
                                break;
                        }
                    }
                }
            }
        }
    };

    @Override
    public void initListener() {
        super.initListener();
        //注册广播接收器，接收器以匿名内部类的方式实例化，所以要记得注销，否则容易内存泄漏
        //注册广播与发送广播都可以使用一个特定的字符串的作为权限标识，设置之后只有相同权限的接收器才能接收到该广播
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("BroadcastTest1");
        intentFilter.addAction("BroadcastTest2");
        getActivity().registerReceiver(receiver, intentFilter);
    }

    @Override
    public void initData() {
        super.initData();
        //Intent作为android四大组件的通信信件，用于存储通信过程中的信息。
        //这里设置action，intent会发给所有接收此action的接收器。
        Intent intent = new Intent("BroadcastTest1");
        //intent添加数据，包含基础类型和字符串、可序列化类型
        intent.putExtra("key1", "stringValue");
        intent.putExtra("key2", 2);
        intent.putExtra("key3", 2.0);
        //设置intent发往什么包名程序，若设置了，这个intent就只有该包名程序可以接收到，若不设置，所有程序都可以接收到
        intent.setPackage(getActivity().getPackageName());
        getActivity().sendBroadcast(intent);

        //广播还分有序与无序，有序广播接收到可以在接收器中中断，这样后续的广播接收器就无法接收了
//        getActivity().sendOrderedBroadcast(intent,null);
    }

    @Override
    public void onDestroyView() {
        //注销广播接收器，最好在成对存在的生命周期方法中调用，不然重复调用会发生错误（例如未注册却要注销就会有问题）。
        getActivity().unregisterReceiver(receiver);
        super.onDestroyView();
    }
}
