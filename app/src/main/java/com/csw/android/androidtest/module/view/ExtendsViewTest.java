package com.csw.android.androidtest.module.view;

import com.csw.android.androidtest.R;
import com.csw.android.dev_utils.ui.BaseFragment;

/**
 * 通过继承的方式自定义视图,对父类添加功能或改进
 */
public class ExtendsViewTest extends BaseFragment {
    @Override
    public int getContentViewID() {
        return R.layout.fragment_extends_view_test;
    }
}
