package com.csw.android.androidtest.module.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.csw.android.androidtest.R;
import com.csw.android.dev_utils.ui.BaseFragment;
import com.csw.android.dev_utils.utils.ImageLoader;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * 在android中，fragment可以用于提供视图，可以像Activity一样将某个界面封装到fragment，甚至于有人建议将所有
 * 页面都用fragment来写，整个项目只需要一个Activity来显示即可。
 * <p>
 * 使用Fragment时需要主义，View只是fragment的中间产物，在fragment的生命周期中可能会多次生成视图，所以需要注
 * 意及时回收视图和与视图绑定的适配器之类的变量，避免浪费内存。
 * <p>
 * 有人说为了下次createView时显示一样的视图，直接在fragment的生命周期中只生成一次视图并复用，但这样也还是会浪
 * 费内存，只是减少了创建视图和销毁视图的cpu消耗，尽量不要这么做。
 * <p>
 * 缓存视图是非常浪费内存的，特别是视图里面有大量的imageView加载了图片的情况下。正确的做法应该是保存这个页面的
 * 数据到fragment变量中，不用跟随视图销毁，当重新创建视图时，利用已有的数据去初始化ui，就不用重新加载数据了。
 */
public class FragmentForUITest extends BaseFragment {
    @Override
    public int getContentViewID() {
        return R.layout.fragment_fragment_for_ui_test;
    }

    @Override
    public void initData() {
        super.initData();
        //使用fragment应当注意，若fragment已经存在实例（之前已经添加过在fragmentManager中），则应该先通过查
        //找来使用fragment，以减少不必要的实例创建。
        ImageFragment imageFragment = (ImageFragment) getChildFragmentManager().findFragmentByTag(ImageFragment.class.getName());
        if (imageFragment == null) {
            imageFragment = new ImageFragment();
        }
        if (!imageFragment.isAdded()) {
            getChildFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, new ImageFragment(), ImageFragment.class.getName())
                    .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                    .commitAllowingStateLoss();
        } else if (imageFragment.isDetached()) {
            getChildFragmentManager().beginTransaction()
                    .attach(imageFragment)
                    .commitAllowingStateLoss();
        }
    }

    public static final class ImageFragment extends Fragment {
        private String pictureUrl;
        private ImageView imageView;

        @Nullable
        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            imageView = new ImageView(container.getContext());
            imageView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            imageView.setAdjustViewBounds(true);
            initData();
            return imageView;
        }

        private void initData() {
            //有数据则更新ui，没数据就加载数据更新ui
            if (pictureUrl == null) {
                Observable.create(new ObservableOnSubscribe<String>() {
                    @Override
                    public void subscribe(@io.reactivex.annotations.NonNull ObservableEmitter<String> emitter) throws Exception {
                        Thread.sleep(2000);
                        if (!emitter.isDisposed()) {
                            emitter.onNext("https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=2608191236,3997709087&fm=26&gp=0.jpg");
                        }
                    }
                }).subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Consumer<String>() {
                            @Override
                            public void accept(String s) throws Exception {
                                pictureUrl = s;
                                ImageLoader.INSTANCE.loadImage(
                                        this,
                                        imageView,
                                        pictureUrl,
                                        0,
                                        null
                                );
                            }
                        });
            } else {
                ImageLoader.INSTANCE.loadImage(
                        this,
                        imageView,
                        pictureUrl,
                        0,
                        null
                );
            }
        }

        @Override
        public void onDestroyView() {
            imageView = null;
            super.onDestroyView();
        }
    }

}
