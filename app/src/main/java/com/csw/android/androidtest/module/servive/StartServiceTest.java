package com.csw.android.androidtest.module.servive;

import com.csw.android.androidtest.service.StartService;
import com.csw.android.androidtest.ui.LogViewFragment;
import com.csw.android.dev_utils.utils.RxBus;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;

/**
 * 以startService的方式启动服务
 */
public class StartServiceTest extends LogViewFragment {

    @Override
    public void initListener() {
        super.initListener();
        addLifecycleTask(
                RxBus.Companion.getDefault().toObservable(StartService.OnCommandResult.class)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Consumer<StartService.OnCommandResult>() {
                            @Override
                            public void accept(StartService.OnCommandResult onCommandResult) throws Exception {
                                logView.addLog(onCommandResult.getData());
                            }
                        })
        );
    }

    @Override
    public void initData() {
        super.initData();
        StartService.command1("12345");
        StartService.command2();
        StartService.command1("last command");
    }


    @Override
    public void onDestroyView() {
        StartService.stop();
        super.onDestroyView();
    }
}
