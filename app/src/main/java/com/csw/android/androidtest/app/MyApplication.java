package com.csw.android.androidtest.app;

import android.app.Application;

import com.csw.android.dev_utils.SDK;

public class MyApplication extends Application {

    public static MyApplication instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        SDK.INSTANCE.init(this);
    }
}
