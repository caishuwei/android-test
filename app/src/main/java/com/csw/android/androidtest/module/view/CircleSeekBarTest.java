package com.csw.android.androidtest.module.view;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;

import com.csw.android.androidtest.R;
import com.csw.android.androidtest.view.CircleSeekBar;
import com.csw.android.dev_utils.ui.BaseFragment;
import com.csw.android.dev_utils.utils.LogUtils;
import com.csw.android.dev_utils.utils.ScreenInfo;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

/**
 * 圆形的SeekBar实现
 */
public class CircleSeekBarTest extends BaseFragment {
    private List<CircleSeekBar> otherCircleSeekBar = new ArrayList<>();
    private CircleSeekBar circleSeeBar;

    @Override
    public int getContentViewID() {
        return R.layout.fragment_circle_seek_bar;
    }

    @Override
    public void initView(@NotNull View rootView, @Nullable Bundle savedInstanceState) {
        super.initView(rootView, savedInstanceState);
        otherCircleSeekBar.add(rootView.findViewById(R.id.circleSeeBar1));
        otherCircleSeekBar.add(rootView.findViewById(R.id.circleSeeBar2));
        otherCircleSeekBar.add(rootView.findViewById(R.id.circleSeeBar3));
        otherCircleSeekBar.add(rootView.findViewById(R.id.circleSeeBar4));
        otherCircleSeekBar.add(rootView.findViewById(R.id.circleSeeBar5));
        otherCircleSeekBar.add(rootView.findViewById(R.id.circleSeeBar6));
        otherCircleSeekBar.add(rootView.findViewById(R.id.circleSeeBar7));
        otherCircleSeekBar.add(rootView.findViewById(R.id.circleSeeBar8));
        circleSeeBar = rootView.findViewById(R.id.circleSeeBar);
        circleSeeBar.setProgressStartColor(Color.WHITE);
        circleSeeBar.setProgressEndColor(Color.BLACK);
        circleSeeBar.setRingWidth(ScreenInfo.INSTANCE.dp2Px(40));
        circleSeeBar.setMax(360);
        circleSeeBar.setProgress(0);
        circleSeeBar.setSliderColor(Color.WHITE);
        circleSeeBar.setSliderSize(ScreenInfo.INSTANCE.dp2Px(30));
    }

    @Override
    public void initListener() {
        super.initListener();
        circleSeeBar.setOnSeekBarChangeListener(new CircleSeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(CircleSeekBar seekBar, int progress, boolean fromUser) {
                float ratio = progress * 1f / seekBar.getMax();
                int color = Color.rgb((int) (255 * ratio), (int) (255 * ratio), (int) (255 * ratio));
                circleSeeBar.setSliderColor(color);
//                circleSeeBar.setProgressEndColor(color);
                LogUtils.INSTANCE.d(seekBar, "progress " + progress);
                for (CircleSeekBar csb : otherCircleSeekBar) {
                    csb.setProgress(progress);
                }
            }

            @Override
            public void onStartTrackingTouch(CircleSeekBar seekBar) {
                circleSeeBar.setSliderSize(ScreenInfo.INSTANCE.dp2Px(40));
            }

            @Override
            public void onStopTrackingTouch(CircleSeekBar seekBar) {
                circleSeeBar.setSliderSize(ScreenInfo.INSTANCE.dp2Px(30));
            }
        });
    }

    @Override
    public void onDestroyView() {
        circleSeeBar = null;
        otherCircleSeekBar = null;
        super.onDestroyView();
    }
}
