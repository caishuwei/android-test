package com.csw.android.androidtest.module.thread;

import android.view.View;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Executor意为执行器，用于执行任务。
 * ExecutorService继承自Executor，用于提供任务控制，比如中断执行。
 * ThreadPoolExecutor实现了ExecutorService，可以通过这个类创建一个线程池用于任务调度。
 * Executors是创建执行器的工具类，可以快速创建常用的线程池。
 */
public class ExecutorsTest extends ThreadTest {

    //单一线程池
//    private ExecutorService executor = Executors.newSingleThreadExecutor();
    //固定线程数量的线程池，新任务优先分配给空余线程，若无线程空余，则添加到队列等待有线程空闲时来接取任务
//    private ExecutorService executor = Executors.newFixedThreadPool(5);
    //缓存线程池，当添加任务时会创建线程去处理，最大的线程数量为Integer.maxValue，若线程执行完会等候一分钟，
    //若无任务分配则结束线程，所以同一时间任务量大的时候会创建很多线程去处理，任务都执行完会缓存线程一分钟，过了一分钟就关闭线程腾出内存。
//    private ExecutorService executor = Executors.newCachedThreadPool();
    //调度线程池，可指定核心线程池数量，提交的任务超过核心线程池数量则进入等待队列，这个与newFixedThreadPool很类似
    //不过强在调度上，方法实际返回的是ScheduledThreadPoolExecutor，可以通过execute方法设置任务延迟多少毫秒执行。
    private ExecutorService executor = Executors.newScheduledThreadPool(20);

    //常见的线程池并不总能满足我们的需求，可以结合多种线程池来实现，比如用固定线程池和缓存线程池，当固定线程池
    //执行任务数量等于最大的线程数量时，放到缓存线程池去执行，这样就可以实现常驻几个线程处理任务，若任务增多则
    //用缓存线程池来处理，过后空闲时释放。结合实际情况来使用，线程的创建和销毁还是比较耗费资源的吧


    @Override
    public void initListener() {
        super.initListener();
        run.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logs = new StringBuffer();
                appendLog(null);
                for (int i = 1; i <= 40; i++) {
                    final int task = i;
                    executor.execute(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            appendLog(task + "");
                        }
                    });
                }
            }
        });
    }

    private void appendLog(String taskName) {
        synchronized (this) {
            if(executor instanceof ThreadPoolExecutor ){
                ThreadPoolExecutor tpe = (ThreadPoolExecutor) executor;
                logs.append(tpe.getPoolSize());
                logs.append("_");
                logs.append(tpe.getMaximumPoolSize());
                logs.append("___>");
            }
            logs.append(" runOn___>");
            logs.append(Thread.currentThread().getName());
            if (taskName != null) {
                logs.append(" task ");
                logs.append(taskName);
            }
            logs.append("\n");
            updateLog();
        }
    }

    @Override
    public void onDestroy() {
        //立即关闭，未执行的任务不再执行。
//        executor.shutdownNow();
        //关闭线程池，已提交的任务会执行完。
        executor.shutdown();
        super.onDestroy();
    }
}
