package com.csw.android.androidtest.service;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;

import androidx.annotation.Nullable;

import com.csw.android.androidtest.app.MyApplication;
import com.csw.android.dev_utils.utils.RxBus;

/**
 * Service属于android的四大组件之一，作为后台处理任务的代表，在早期被各种滥用（特别是消息推送），大量的后台程
 * 序导致android系统卡顿，手机发热，待机电量消耗快等各种影响，后来android和国内各厂商对系统进行优化，对各种常
 * 驻后台方式赶尽杀绝，限制了后台服务的各种功能（例如位置等信息的硬件访问次数），而且系统一旦内存不够很容易被杀
 * 。并且不再允许app处于后台时再开启服务（有过双进程互相启动保活的例子，这里可能是针对它）。
 * 总而言之，服务到今天，被限制颇多，如需使用后台执行功能，可使用前台服务去实现，后台服务逐渐变得鸡肋了。
 * <p>
 * 多次start，服务启动后存在实例，并不会启动多个服务，该实例可以在onStartCommand中处理任务。
 * <p>
 * 使用服务，想保证它可以稳定执行任务，而又不想获取服务的对象，直接使用startService启动服务处理任务，若服务需
 * 要响应，可以通过广播 eventBus rxBus等事件订阅组件进行处理，一般如果是同个进程内的使用那推荐rxBus。
 */
public class StartService extends Service {
    private static final String COMMAND_1 = "COMMAND_1";
    private static final String COMMAND_2 = "COMMAND_2";
    private static final String COMMAND_STOP = "COMMAND_STOP";

    public static void command1(String data) {
        Bundle d = new Bundle();
        d.putString("data", data);
        start(COMMAND_1, d);
    }

    public static void command2() {
        start(COMMAND_2, null);
    }

    public static void stop() {
        start(COMMAND_STOP, null);
    }

    public static void start(String command, Bundle data) {
        Intent intent = new Intent(MyApplication.instance, StartService.class);
        intent.setAction(command);
        if (data != null) {
            intent.putExtras(data);
        }
        MyApplication.instance.startService(intent);
    }


    @Override
    public void onCreate() {
        super.onCreate();
        //状态初始化
        RxBus.Companion.getDefault().post(new OnCommandResult("StartService created"));
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            final String action = intent.getAction();
            if (action != null) {
                switch (action) {
                    case COMMAND_1:
                        String data = intent.getStringExtra("data");
                        execCommand1(data);
                        break;
                    case COMMAND_2:
                        execCommand2();
                        break;
                    case COMMAND_STOP:
                        stopSelf();
                        break;
                }
            }
        }
        return super.onStartCommand(intent, flags, startId);
    }

    private void execCommand1(String data) {
        RxBus.Companion.getDefault().post(new OnCommandResult("StartService is receive " + data));
    }

    private void execCommand2() {
        RxBus.Companion.getDefault().post(new OnCommandResult("StartService execCommand2"));
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        //服务若因为资源不足被关闭时，这里可以保存状态，等候下次启动时恢复。
        super.onDestroy();
    }

    //----------------------------------------------------------------------------------------------
    public static final class OnCommandResult {
        private final String data;

        public OnCommandResult(String data) {
            this.data = data;
        }

        public String getData() {
            return data;
        }
    }
}
