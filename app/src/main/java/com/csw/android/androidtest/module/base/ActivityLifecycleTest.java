package com.csw.android.androidtest.module.base;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;

/**
 * Activity生命周期
 */
public class ActivityLifecycleTest extends Activity {

    public static void open(Context context) {
        context.startActivity(new Intent(context, ActivityLifecycleTest.class));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
}
