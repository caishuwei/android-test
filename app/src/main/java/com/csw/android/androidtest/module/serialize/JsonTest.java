package com.csw.android.androidtest.module.serialize;

import android.view.View;

import com.csw.android.androidtest.ui.LogViewFragment;
import com.csw.android.dev_utils.utils.GSONUtils;
import com.csw.android.dev_utils.utils.LogUtils;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * json，是现在网络通信常用的数据表达方式，序列化方式，其优点是可读性高，同样通过反射，但序列化后的数据结构清晰，
 * 拥有可读性，由于不需要记录类型信息，所以序列化数据占用空间比Serializable少，不过从空间上没人比得过Parcelable,
 * 性能上也应该与Serializable差不多，但都低于Parcelable，但你知道的，有时候可读性比性能更重要，哈哈，所以选择
 * json作为数据序列化反序列化是大多数人的选择。
 */
public class JsonTest extends LogViewFragment {

    @Override
    public void initData() {
        super.initData();
        Observable.create(new ObservableOnSubscribe<TestData>() {
            @Override
            public void subscribe(@NonNull ObservableEmitter<TestData> emitter) throws Exception {
                TestData testData = new TestData("二", 2, 2.0, logView);
                String serializableData = GSONUtils.INSTANCE.toJSONString(testData);
                LogUtils.INSTANCE.d("JsonTest", serializableData.length() * 8 + "");
                TestData deserializableObject = GSONUtils.INSTANCE.parseObject(serializableData, TestData.class);
                if (deserializableObject == null) {
                    if (!emitter.isDisposed()) {
                        emitter.onError(new Exception(serializableData + "无法解析成 TestData"));
                    }
                    return;
                }
                if (!emitter.isDisposed()) {
                    emitter.onNext(deserializableObject);
                }
                if (!emitter.isDisposed()) {
                    emitter.onComplete();
                }
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(new Consumer<TestData>() {
                    @Override
                    public void accept(TestData testData) throws Exception {
                        logView.addLog(testData.v1);
                        logView.addLog(testData.v2);
                        logView.addLog(testData.v3);
                    }
                })
                .subscribe();
    }

    public static class TestData {

        private final String v1;
        private final int v2;
        private final double v3;

        //transient关键字标记该属性不参与序列化反序列化
        transient private final View logView;

        public TestData(String v1, int v2, double v3, View logView) {
            this.v1 = v1;
            this.v2 = v2;
            this.v3 = v3;
            this.logView = logView;
        }

        public String getV1() {
            return v1;
        }

        public int getV2() {
            return v2;
        }

        public double getV3() {
            return v3;
        }
    }
}
