package com.csw.android.androidtest.module.view;

import android.os.Bundle;
import android.view.View;

import com.csw.android.androidtest.R;
import com.csw.android.androidtest.view.CustomTitleBar;
import com.csw.android.dev_utils.ui.BaseFragment;

import org.jetbrains.annotations.NotNull;

import java.util.concurrent.Executors;

/**
 * 通过组合视图进行自定义
 */
public class ComposeViewTest extends BaseFragment {

    private CustomTitleBar customTitleBar;

    @Override
    public int getContentViewID() {
        return R.layout.fragment_compose_view_test;
    }

    @Override
    public void initView(@NotNull View rootView, @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.initView(rootView, savedInstanceState);
        customTitleBar = rootView.findViewById(R.id.customTitleBar);
    }

    @Override
    public void initData() {
        super.initData();
        customTitleBar.setTitle(this.getClass().getSimpleName());
    }

    @Override
    public void onDestroyView() {
        customTitleBar = null;
        super.onDestroyView();
    }


}
