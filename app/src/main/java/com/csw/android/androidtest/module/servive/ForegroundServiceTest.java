package com.csw.android.androidtest.module.servive;

import com.csw.android.androidtest.service.ForegroundService;
import com.csw.android.androidtest.ui.LogViewFragment;
import com.csw.android.dev_utils.utils.RxBus;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;

/**
 * 前台服务测试
 */
public class ForegroundServiceTest extends LogViewFragment {

    @Override
    public void initListener() {
        super.initListener();
        addLifecycleTask(
                RxBus.Companion.getDefault().toObservable(ForegroundService.OnCommandResult.class)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Consumer<ForegroundService.OnCommandResult>() {
                            @Override
                            public void accept(ForegroundService.OnCommandResult onCommandResult) throws Exception {
                                logView.addLog(onCommandResult.getData());
                            }
                        })
        );
    }

    @Override
    public void initData() {
        super.initData();
        ForegroundService.command1("12345");
        ForegroundService.command2();
        ForegroundService.command1("last command");
    }


    @Override
    public void onDestroyView() {
        ForegroundService.stop();
        super.onDestroyView();
    }

}
