package com.csw.android.androidtest.module.view;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.csw.android.androidtest.R;
import com.csw.android.dev_utils.ui.BaseFragment;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * 自定义视图步骤测试
 */
public class CustomViewStepTest extends BaseFragment {
    private ImageView imageView;
    private Thread thread;

    private Runnable bitmapGenerateTask = new Runnable() {
        @Override
        public void run() {
            try {
                //给定一个TextView，将其转换成一张160px宽度的图片。
                TextView textView = new TextView(getContext());
                textView.setBackgroundColor(Color.BLUE);
                textView.setTextColor(Color.BLACK);
                textView.setTextSize(14);
                textView.setText("互联网思维不再局限于互联网。就像人类历史上的“文艺复兴”一样，这种思想的核心即将传播开来，这将对整个时代产生深远的影响。这本书是深入研究互联网思维的精髓。作者以简单的方式集中论述了24种网络思维的核心和精神，并以实例逐一对这24种网络思维进行了评述。它为个人和企业掌握互联网思维在工作、生活和商业中的巨大创新和机遇，如何在互联网思维下运作，以及如何利用互联网思维进行升级和改造提供了最实用的指导。\n" +
                        "\n" +
                        "今天我们要讲的这本书是《一本书读懂24种互联网思维》，作者是安杰，建国60周年中国策划功勋人物、安杰智扬营销策划与投资管理机构董事长。他是中国时尚产业“百亿模式”及“O2O模式”研究开创者。\n" +
                        "\n" +
                        "\n" +
                        "什么是互联网思维\n" +
                        "\n" +
                        "在品牌主权的时代，打“漏斗销售”，比如在央视投放轰炸广告，只能“打击”一些客户；在消费者主权的时代，我们必须首先成为忠诚的顾客，用铁杆粉丝影响更多的粉丝。传统品牌的玩法，打八环和九环已经很棒了；在移动互联网时代，你必须打十环！也就是说，你的产品，你的购买方式，你的销售语言，等等。应该有“直击靶心”的思维和演奏风格。关键词“直接命中”可以搜寻几乎所有的互联网思维，包括产品思维、爆点思维、简约思维、痛点思维、免费思维等等。\n" +
                        "\n");
                //测量
                textView.measure(
                        View.MeasureSpec.makeMeasureSpec(160, View.MeasureSpec.EXACTLY),
                        View.MeasureSpec.makeMeasureSpec(10000, View.MeasureSpec.AT_MOST)
                );
                //布局
                textView.layout(
                        0,
                        0,
                        textView.getMeasuredWidth(),
                        textView.getMeasuredHeight()
                );
                //绘制
                final Bitmap bm = Bitmap.createBitmap(
                        textView.getMeasuredWidth(),
                        textView.getMeasuredHeight(),
                        Bitmap.Config.ARGB_8888
                );
                Canvas canvas = new Canvas(bm);
                textView.draw(canvas);
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        imageView.setImageBitmap(bm);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    public int getContentViewID() {
        return R.layout.fragment_custom_view_step_test;
    }

    @Override
    public void initView(@NotNull View rootView, @Nullable Bundle savedInstanceState) {
        super.initView(rootView, savedInstanceState);
        imageView = rootView.findViewById(R.id.imageView);
    }

    @Override
    public void initData() {
        super.initData();

        thread = new Thread(bitmapGenerateTask);
        thread.start();
    }

    @Override
    public void onDestroyView() {
        thread.interrupt();
        thread = null;
        imageView = null;
        super.onDestroyView();
    }
}
