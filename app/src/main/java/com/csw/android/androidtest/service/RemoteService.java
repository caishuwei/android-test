package com.csw.android.androidtest.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;

import androidx.annotation.Nullable;

import com.csw.android.androidtest.RemoteServiceAIDLInterface;

/**
 * 采用bindservice+AIDL实现跨进程组件通信
 */
public class RemoteService extends Service {

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return new MyBinder();
    }

    /**
     * 经过编译，自动生成与RemoteServiceAIDLInterface相关的抽象类，继承并实现功能
     */
    private class MyBinder extends RemoteServiceAIDLInterface.Stub {

        @Override
        public String execCommand1(String data) throws RemoteException {
            return "RemoteService is receive " + data;
        }

        @Override
        public String execCommand2() throws RemoteException {
            return "RemoteService execCommand2";
        }
    }

}
